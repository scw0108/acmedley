import React, { useState } from "react";
import { Link } from "react-router-dom";
import { GooLogin } from "./GoogleLogin";

const Header: React.FC = () => {
  const [menuOpen, setMenuOpen] = useState(false);
  const [showDropdown, setShowDropdown] = useState(false);
  const picture = sessionStorage.getItem("picture");

  const handleMenuClick = (): void => {
    setMenuOpen((prevOpen) => !prevOpen);
  };

  const handleDropdownClick = (): void => {
    setShowDropdown((prevDropDown) => !prevDropDown);
  };

  const handleLogout = (): void => {
    setShowDropdown((prevDropDown) => !prevDropDown);
    sessionStorage.removeItem("picture");
    sessionStorage.removeItem("user_name");
    sessionStorage.removeItem("user_mail");
    sessionStorage.removeItem("token");
    window.location.reload();
  };

  return (
    <div className="bg-header">
      <header className="ml-16 h-16 w-11/12 bg-header">
        <div className="ml-14 flex h-full items-center justify-between px-4 pr-14">
          <div
            data-testid="menu-button"
            className={`group cursor-pointer space-y-1.5 rounded p-2.5 hover:bg-nav ${
              menuOpen ? "bg-nav" : ""
            }`}
            onClick={handleMenuClick}
          >
            <span
              data-testid="hamburger-menu-line"
              className={`block h-1 w-8 group-hover:bg-header  ${
                menuOpen ? "bg-header" : "bg-nav"
              }`}
            ></span>
            <span
              data-testid="hamburger-menu-line"
              className={`block h-1 w-8 group-hover:bg-header  ${
                menuOpen ? "bg-header" : "bg-nav"
              }`}
            ></span>
            <span
              data-testid="hamburger-menu-line"
              className={`block h-1 w-8 group-hover:bg-header  ${
                menuOpen ? "bg-header" : "bg-nav"
              }`}
            ></span>
          </div>
          <Link to="/">
            <img
              src={"/logo.png"}
              alt="Logo"
              className="h-8 w-8 cursor-pointer"
            />
          </Link>
          <div>
            {picture !== null && picture !== undefined ? (
              <div className="relative inline-block">
                <img
                  src={picture}
                  alt=""
                  className="h-12 w-12 cursor-pointer rounded-full"
                  onClick={handleDropdownClick}
                />
                {showDropdown && (
                  <div className="absolute right-0 z-10 mr-14 mt-2 w-24 rounded bg-nav p-2 opacity-95 shadow transition-opacity duration-300">
                    <div
                      className="flex cursor-pointer items-center px-4 py-2 text-left text-sm hover:bg-gray_limpid"
                      onClick={handleLogout}
                    >
                      登出
                    </div>
                  </div>
                )}
              </div>
            ) : (
              <GooLogin />
            )}
          </div>
        </div>
        {menuOpen && (
          <div data-testid="nav-menu" className="relative ml-14 mt-2 w-screen">
            <div className="absolute left-6 z-10 w-auto rounded-2xl bg-nav p-3">
              <div
                data-testid="menu-first-option"
                className="cursor-pointer rounded-xl bg-nav px-4 py-1 text-lg font-semibold hover:bg-gray_limpid"
              >
                <Link to="/owner">我主辦的活動</Link>
              </div>
              <div
                data-testid="menu-second-option"
                className="mt-1.5 cursor-pointer rounded-xl bg-nav px-4 py-1 text-lg font-semibold hover:bg-gray_limpid"
              >
                <Link to="/participant">我參加的活動</Link>
              </div>
            </div>
          </div>
        )}
      </header>
    </div>
  );
};

export default Header;
