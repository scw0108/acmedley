import React from "react";
import { render, screen } from "@testing-library/react";
import { MemoryRouter, Route, Routes } from "react-router-dom";
import ActivityDetailPage from "../pages/ActivityDetailPage";
import "@testing-library/jest-dom/extend-expect"; // Import the extend-expect utility

jest.mock("node-fetch");

describe("ActivityDetailPage", () => {
  beforeEach(() => {
    jest.resetModules();
  });

  it("renders activity details", async () => {
    // Mock the fetch response
    const mockResponse = {
      ID: 2,
      ANAME: "Act2",
      A_STIME: "2020-12-10T14:00:50.000Z",
      A_ETIME: "2020-12-20T14:00:50.000Z",
      R_STIME: "2020-11-10T14:00:50.000Z",
      R_ETIME: "2020-11-20T14:00:50.000Z",
      DESCRIPT: "test1",
      LOCAT: "nccu",
      MGR_UEMAIL: "Andy@gmail.com"
    };
    global.fetch = jest.fn().mockResolvedValue({
      json: jest.fn().mockResolvedValue(mockResponse)
    }) as any;

    render(
      <MemoryRouter initialEntries={["/activity/2/owner"]}>
        <Routes>
          <Route
            path="/activity/:activityId/owner"
            element={<ActivityDetailPage />}
          />
        </Routes>
      </MemoryRouter>
    );

    await screen.findAllByRole("img");
    const activityPicture = screen.getAllByRole("img");
    expect(activityPicture).toHaveLength(2);
    const memberIdElement = screen.getByText((content, element) => {
      return content.includes("nccu");
    });
    expect(memberIdElement).toBeInTheDocument();
    const activityPictureComponents = screen.getAllByText("Act2");
    expect(activityPictureComponents).toHaveLength(2);
  });
});
