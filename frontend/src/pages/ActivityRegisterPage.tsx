import React, { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import timesplit from "../utils/TimeSplit";
import api from "../utils/api";
import Header from "../components/Header";
interface Activity {
  ID: number;
  ANAME: string;
  A_STIME: string;
  A_ETIME: string;
  R_STIME: string;
  R_ETIME: string;
  DESCRIPT: string;
  LOCAT: string;
  MGR_UEMAIL: string;
  IMAGES: string;
}
export default function ActivityRegisterPage(): JSX.Element {
  const id = useParams().activityId;
  const navigate = useNavigate();
  const [activity, setActivities] = useState<Activity>();
  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");

  useEffect(() => {
    api
      .ActivityList(String(id))
      .then((res) => {
        const activity: Activity = {
          ID: res.ID,
          ANAME: res.ANAME,
          A_STIME: res.A_STIME,
          A_ETIME: res.A_ETIME,
          R_STIME: res.R_STIME,
          R_ETIME: res.R_ETIME,
          DESCRIPT: res.DESCRIPT,
          LOCAT: res.LOCAT,
          MGR_UEMAIL: res.MGR_UEMAIL,
          IMAGES: res.IMAGES
        };
        setActivities(activity);
      })
      .catch((error) => {
        console.error(error);
      });
  }, [id]);
  if (activity === undefined) {
    return <div>Wrong</div>;
  }
  let ADate = null;
  const [ASDate, ASTime] = timesplit(activity.A_STIME);
  const [AEDate, AETime] = timesplit(activity.A_ETIME);
  if (ASDate === AEDate) {
    ADate = ASDate + " " + ASTime;
  } else {
    ADate = ASDate + " (" + ASTime + ") ~ " + AEDate + " (" + AETime + ")";
  }
  const handleNameChange = (event: any): void => {
    setName(event.target.value);
  };

  const handlePhoneChange = (event: any): void => {
    setPhone(event.target.value);
  };

  const handleSubmit = (event: any): void => {
    event.preventDefault();
    // 在這裡使用name, phone, email進行所需的處理
    const phoneRegex = /^\d{10}$/; // 10位數字
    if (phone.match(phoneRegex) == null) {
      alert("請輸入有效的電話號碼");
      return; // 停止處理
    }

    alert("報名成功");
    console.log(id, name, phone);
    void api.createRegister(String(id), name, phone);
    // 清空輸入框
    setName("");
    setPhone("");

    navigate(-1);
  };

  return (
    <div>
      <Header />
      <div
        className="min-h-screen px-20 py-10"
        style={{ backgroundColor: "#fffef0" }}
        key={activity.ID}
      >
        <div className="pl-16 font-noto_sans text-2xl">{activity.ANAME}</div>
        <hr className="my-7 ml-16 w-11/12 border-2 border-t border-gray" />
        <div
          className="mb-96 me-12 ms-14 flex justify-center rounded-xl"
          style={{ backgroundColor: "#f8f2dd" }}
        >
          <div className="h-auto w-full rounded-md p-5 font-noto_sans">
            <div className="flex justify-between">
              <div>
                <h1 className="text-xl">&#128205;日期及地點</h1>
                <p className="mt-3 px-5 text-lg">日期:&emsp;{ADate}</p>
                <p className="px-5 text-lg">地點:&emsp;{activity.LOCAT}</p>
                <div className="flex justify-between ">
                  <div>
                    <p className="mt-3 px-5 text-lg">
                      姓名:&emsp;
                      <input
                        className="w-full border-b border-gray"
                        style={{ backgroundColor: "#f8f2dd" }}
                        type="text"
                        value={name}
                        onChange={handleNameChange}
                      />
                    </p>
                  </div>
                  <div>
                    <p className="ml-96 mt-3 pr-5 text-lg">
                      聯絡電話:&emsp;
                      <input
                        className="mr-8 w-full border-b border-gray"
                        style={{ backgroundColor: "#f8f2dd" }}
                        type="text"
                        pattern="[0-9]*"
                        maxLength={10}
                        value={phone}
                        onChange={handlePhoneChange}
                      />
                    </p>
                  </div>
                </div>

                <div className="mt-10 flex justify-center">
                  <button
                    className="rounded-lg bg-amber-300 px-4 py-2 font-bold text-white hover:bg-yellow-600"
                    onClick={handleSubmit}
                  >
                    確定送出
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
