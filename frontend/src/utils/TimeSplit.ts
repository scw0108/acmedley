export default function timesplit(dateTime: string): string[] {
  const [date, time]: string[] = dateTime.split("T");
  const timeParts = time.split(":");
  const hour = timeParts[0];
  const minute = timeParts[1];
  const Time = hour + ":" + minute;

  return [date, Time];
}
