# Introduction
AcMedley is a centralized platform for managing and participating in various activities and events.
This is an English document of [AcMedley](https://gitlab.com/kylai02/acmedley).
## Purpose
Information about various activities is often scattered across different platforms—some on Facebook, some on Instagram, and some even on Google Forms. Therefore, this platform can integrate all activity information, making it convenient for users and event organizers to access.
## Role Description
- **General user**: View event information and register for their favorite activities
- **Event organizers**: Create new events and view participants

## Features
- Centralized event information
- Event creation and management
- User registration for events
- Participant tracking for organizers

## Technology Stack
### Frontend
- React
### Backend
- Express
### Database
- MySQL
### DevOps
- GitLab
- Docker
- More CICD information:
    * [CI/CD Configuration](https://gitlab.com/scw0108/acmedley/-/ci/editor)
    * [Original Project Pipeline](https://gitlab.com/kylai02/acmedley/-/pipelines)

### Architecture
The application follows a typical web application architecture:
![Architecture](./Architecture.png)

# Instructions

## Installation

### `npm install --legacy-peer-deps`

Add `--legacy-peer-deps` to avoid dependency issues

## Local Testing

### `npm start`

Start the React App

### `npm run test`
Run unit tests (jest)

### `npm run lint`

Run semantic check (eslint) and style check (prettier)

## Test Coverage

### Frontend
`npm test -- --coverage --watchAll=false`
![frontend_coverage](./frontend_coverage.png)

### Backend
`npm test -- --coverage`
![backend_coverage](./backend_coverage.png)

## Import Test Database

### `mysql -u <username> -p <database_name> < testdb.sql`
or
### `mysql -u <username> -p`
### `source <full path>/ACMEDLEY/database/testdb.sql;`

## Backend DEV

- Login: `curl -X POST -H "Content-Type: application/json" -d '{"user_name": <user name>, "user_mail":<user email>}'  http://localhost:8000/account/login`

- Create the Registration of an activity: `curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer <token>" -d '{"name":<name>, "phone":<phone num>, "email":<email>}' http://localhost:8000/api/1.0/activity/register/create\?activityId=<ID>`

- Update the Registration of an activity: `curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer <token>" -d '{"name":<name>, "phone":<phone num>, "email":<email>}' http://localhost:8000/api/1.0/activity/register/update\?activityId=<ID>`
