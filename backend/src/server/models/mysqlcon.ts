import mysql from "mysql2/promise";
import * as dotenv from "dotenv";
dotenv.config();
const { DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE } = process.env;

const mysqlConfig = {
  host: DB_HOST,
  user: DB_USERNAME,
  password: DB_PASSWORD,
  database: DB_DATABASE
};

const mysqlEnv = {
  ...mysqlConfig,
  waitForConnections: true,
  connectionLimit: 20
};

const pool = mysql.createPool(mysqlEnv);

export default pool;
