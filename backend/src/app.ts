import express from "express";
import cors from "cors";
import type {
  Application,
  Request,
  Response,
  ErrorRequestHandler
} from "express";
import * as dotenv from "dotenv";
import router from "./server/routes/activity_route";
import accountRouter from "./server/routes/user_route";

dotenv.config();
const { API_VERSION } = process.env;

const app: Application = express();

app.set("json spaces", 2);
app.use(express.json());
// app.use(cors());
app.use(cors({ origin: "http://localhost:3000" }));

app.get("/", (req: Request, res: Response) => {
  res.json({
    message: "Hello Express + TS!!~~"
  });
});

app.use("/api/" + String(API_VERSION), [router]);

app.use("/account", [accountRouter]);

app.use(function (req, res) {
  res.status(404).send({ error: "Invalid Path" });
});

// About the Type Declaration, follows
// https://stackoverflow.com/questions/50218878/typescript-express-error-function
const errorHandler: ErrorRequestHandler = (err, req, res, next) => {
  console.log(err);
  res.status(500).send("Internal Server Error");
};

app.use(errorHandler);

export default app;
