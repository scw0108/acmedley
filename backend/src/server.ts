import * as dotenv from "dotenv";
import app from "./app";

dotenv.config();
const { PORT } = process.env;

app.listen(PORT, () => {
  console.log(`Now listening on port ${String(PORT)}...`);
});
