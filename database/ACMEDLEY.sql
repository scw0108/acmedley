-- MySQL dump 10.13  Distrib 8.0.31, for Linux (x86_64)
--
-- Host: localhost    Database: ACMEDLEY
-- ------------------------------------------------------
-- Server version	8.0.31-0ubuntu0.20.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ACTIVITY`
--

DROP TABLE IF EXISTS `ACTIVITY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ACTIVITY` (
  `ID` varchar(50) NOT NULL,
  `ANAME` varchar(127) NOT NULL,
  `A_STIME` datetime DEFAULT NULL,
  `A_ETIME` datetime DEFAULT NULL,
  `R_STIME` datetime DEFAULT NULL,
  `R_ETIME` datetime DEFAULT NULL,
  `DESCRIPT` varchar(1023) DEFAULT NULL,
  `LOCAT` varchar(127) DEFAULT NULL,
  `MGR_UEMAIL` varchar(127) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `MGR_UEMAIL` (`MGR_UEMAIL`),
  CONSTRAINT `ACTIVITY_ibfk_1` FOREIGN KEY (`MGR_UEMAIL`) REFERENCES `USER` (`UEMAIL`),
  CONSTRAINT `ACTIVITY_chk_1` CHECK (((`A_STIME` <= `A_ETIME`) and (`R_STIME` <= `R_ETIME`) and (`A_STIME` >= `R_ETIME`)))
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACTIVITY`
--

LOCK TABLES `ACTIVITY` WRITE;
/*!40000 ALTER TABLE `ACTIVITY` DISABLE KEYS */;
INSERT INTO `ACTIVITY` VALUES ('10','親子野餐日Family Picnic!','2023-08-05 10:00:00','2023-08-05 14:00:00','2023-07-15 09:00:00','2023-08-01 18:00:00','在新北市石碇區風景優美的公園舉辦的親子野餐日，有\r\n    草地遊戲、手工工作坊、親子競賽和美食分享等活動，讓家庭度過美好的一天。','新北市石碇區忠貞路56號','Andy@gmail.com'),('11','夜間攝影之旅','2023-09-10 20:00:00','2023-09-11 02:00:00','2023-08-15 10:00:00','2023-09-05 18:00:00','在台中市西區舉辦的夜間攝影之旅，參與者將在專業攝影師的指導下，\r\n    探索夜晚的美景並學習如何拍攝出令人驚艷的夜景照片。','台中市西區忠明南路20號','Andy@gmail.com'),('12','⟪三生有歌⟫音樂創作工作坊','2023-10-20 11:00:00','2023-10-21 15:00:00','2023-09-25 14:00:00','2023-10-15 18:00:00','喜歡音樂或多媒體創作的你，不要錯過十月舉行的三個「《三\r\n    生有歌》創作工作坊」！工作坊的主題均圍繞歌頌生命，分別\r\n    以音樂、文字、多媒體創作作為媒介，並由香港著名音樂人馮\r\n    穎琪、填詞人王樂儀，以及多媒體創作人陳浩然帶領，為一眾\r\n    對創作充滿抱負的青年音樂人及多媒體創作人帶來千載難逢的\r\n    學習機會。','台南市北區中正路78號','Andy@gmail.com'),('13','高雄美食大探險','2023-11-05 09:00:00','2023-11-06 17:00:00','2023-10-10 10:00:00','2023-11-01 18:00:00','美食探險之旅，參與者將品嚐各種高雄道地美食，並了解背後的文化故事和烹飪技巧','高雄市左營區博愛二路168號','Andy@gmail.com'),('14','SUP YOGA瑜珈海灘派對','2023-12-01 09:00:00','2023-12-01 17:00:00','2023-11-01 10:00:00','2023-11-25 18:00:00','在美麗的海灘上進行瑜珈課程，並享受放鬆身心的海灘派對氛圍!','宜蘭縣頭城鎮合興路66號','Andy@gmail.com'),('15','JCCAC手作市集','2024-01-15 10:00:00','2024-01-15 16:00:00','2023-12-10 09:00:00','2024-01-10 17:00:00','參與者可以購買各種手工製品、參加手作工作坊並展示自己的創作。','台中市南區建國南路2號','Andy@gmail.com'),('16','NCCU職涯講座','2023-12-20 13:00:00','2023-12-20 16:00:00','2023-12-05 09:00:00','2023-12-17 17:00:00','有請好棒棒講師來到商院進行演講，主要為提供商願相關科系的同學\r\n    未來的職涯方向，中間預計休息15分鐘，報名報到者可領取餐盒，最後\r\n    會開放QA時間','116台北市文山區指南路二段64號/國政治大學商學院','Andy@gmail.com'),('17','聖誕夜FUN玩桌遊','2023-12-24 12:00:00','2023-12-24 21:30:00','2023-12-10 09:00:00','2023-12-20 17:00:00','歡迎聖誕夜在政大的同學一起來同樂（現場備有零食、飲料，需脫鞋）','116台北市文山區指南路二段64號/國政治大學藝文中心','Andy@gmail.com'),('6','112年度夏令營兒童藝文研習-想不到的大改造','2023-08-01 13:00:00','2023-08-04 16:00:00','2023-07-21 00:00:00','2023-07-27 16:00:00','運用各式媒材透過組合堆疊、切割折法等不同手法呈現立體作品。報名費900元、材料費450元','葫蘆墩文化中心','Andy@gmail.com'),('7','弦耕雅韻-五美祈安 古琴音樂會','2023-07-22 14:30:00','2023-07-22 16:00:00','2023-07-19 00:00:00','2023-07-21 16:00:00','古琴，為中華文化之精粹。一張七弦，十三枚徽，張弛著幾千年來風雲變幻之蒼穹、孕育所有生命之大地，以及在其間展現精彩生命力的\r\n    人們，這所有交集織造出的所有故事。古人以琴寄情、禮讚、傳薪，更發揮了時局動盪之際，安定人心的強大力量。全球歷經了為時四年的\r\n    疫情肆虐，期間更爆發國際間的戰火，人心惶惶、惴惴不安。為以古琴純善之音，彈撥出對世間與人們安和的祈求，弦耕琴社將在7/22日，\r\n    以不輟的精純技藝，於港區藝術中心演藝廳將美好的虔敬之樂，呈獻給每位聽眾。','臺中市港區藝術中心 / 臺中市清水區忠貞路21號','Andy@gmail.com'),('8','2023銅鑼鄉桐花祭/春桐漫步旅','2023-04-23 09:00:00','2023-04-23 15:00:00','2023-04-18 00:00:00','2023-04-20 16:00:00','test2活動一-桐心藝文饗宴\r\n    客家歌手-黃珮舒、舊匠樂團主唱-阿智、森林裡的古典樂、樟樹歌謠班，輪番上陣，讓您在森林裡享受悠揚的樂曲\r\n    活動二-好物微市集\r\n    在地好物限量販售，僅在好物微市集\r\n    活動三-桐海秘境茶席(名額有限，額滿為止)\r\n    設計屬於銅鑼生活的茶與食，一起賞桐花品茶香\r\n    活動四-生活美學DIY(名額有限，額滿為止)\r\n    在森林裡，運用來自大自然的產物，帶走最存樸的記憶\r\n    活動五-銅鑼覓桐花\r\n    活動當日找尋指定字樣『桐tungˇ 你ngiˇ 共-下kiung-ha』拍照，上傳留言「我在銅鑼，桐你共下，春桐漫步旅」並打卡銅鑼鄉公所，即可至好物微市集兌換限量好禮','苗栗縣銅鑼鄉雙峰路86-1號','Andy@gmail.com'),('9','夏日狂歡派對','2023-07-15 18:00:00','2023-07-15 22:00:00','2023-06-01 09:00:00','2023-07-10 17:00:00','在夜店舉辦的夏日狂歡派對，有現場\r\n    DJ音樂表演、跳舞、飲料暢飲和派對遊戲等，讓您享受熱情的夏日派對氛圍。','台北市信義區松壽路12號','Andy@gmail.com');
/*!40000 ALTER TABLE `ACTIVITY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ACTIVITY_IMAGE`
--

DROP TABLE IF EXISTS `ACTIVITY_IMAGE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ACTIVITY_IMAGE` (
  `ID` varchar(50) NOT NULL,
  `IMG_PATH` varchar(127) NOT NULL,
  PRIMARY KEY (`ID`,`IMG_PATH`),
  CONSTRAINT `ACTIVITY_IMAGE_ibfk_1` FOREIGN KEY (`ID`) REFERENCES `ACTIVITY` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ACTIVITY_IMAGE`
--

LOCK TABLES `ACTIVITY_IMAGE` WRITE;
/*!40000 ALTER TABLE `ACTIVITY_IMAGE` DISABLE KEYS */;
INSERT INTO `ACTIVITY_IMAGE` VALUES ('10','https://i.imgur.com/Nq9sEuT.jpg'),('11','https://i.imgur.com/T3u6hmQ.jpg'),('12','https://i.imgur.com/Kng7aLi.jpg'),('13','https://i.imgur.com/J6xjOpM.jpg'),('14','https://i.imgur.com/TEB4FU8.jpg'),('15','https://i.imgur.com/wsL6Bfk.jpg'),('16','https://i.imgur.com/L07yC8t.jpg'),('17','https://i.imgur.com/4ELlXOT.jpg'),('6','https://i.imgur.com/Yk3IJaq.jpg'),('7','https://i.imgur.com/wnNNWGl.jpg'),('8','https://i.imgur.com/Nwm0lQb.jpg'),('9','https://i.imgur.com/kE0vqSU.jpg');
/*!40000 ALTER TABLE `ACTIVITY_IMAGE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `LEAD`
--

DROP TABLE IF EXISTS `LEAD`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `LEAD` (
  `UEMAIL` varchar(127) NOT NULL,
  `ID` varchar(50) NOT NULL,
  PRIMARY KEY (`UEMAIL`,`ID`),
  KEY `ID` (`ID`),
  CONSTRAINT `LEAD_ibfk_2` FOREIGN KEY (`UEMAIL`) REFERENCES `USER` (`UEMAIL`),
  CONSTRAINT `LEAD_ibfk_3` FOREIGN KEY (`ID`) REFERENCES `ACTIVITY` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `LEAD`
--

LOCK TABLES `LEAD` WRITE;
/*!40000 ALTER TABLE `LEAD` DISABLE KEYS */;
INSERT INTO `LEAD` VALUES ('Andy2@gmail.com','10'),('123456@gmail.com','11');
/*!40000 ALTER TABLE `LEAD` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `REGISTRATION`
--

DROP TABLE IF EXISTS `REGISTRATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `REGISTRATION` (
  `UEMAIL` varchar(127) NOT NULL,
  `RNAME` varchar(127) NOT NULL,
  `UPHONE` varchar(20) NOT NULL,
  `ID` varchar(50) NOT NULL,
  PRIMARY KEY (`UEMAIL`,`ID`),
  KEY `ID` (`ID`),
  CONSTRAINT `REGISTRATION_ibfk_2` FOREIGN KEY (`UEMAIL`) REFERENCES `USER` (`UEMAIL`),
  CONSTRAINT `REGISTRATION_ibfk_3` FOREIGN KEY (`ID`) REFERENCES `ACTIVITY` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `REGISTRATION`
--

LOCK TABLES `REGISTRATION` WRITE;
/*!40000 ALTER TABLE `REGISTRATION` DISABLE KEYS */;
INSERT INTO `REGISTRATION` VALUES ('123456@gmail.com','Alice 黃','0911222333','6'),('Andy@gmail.com','李承恩','0977888999','7');
/*!40000 ALTER TABLE `REGISTRATION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `USER`
--

DROP TABLE IF EXISTS `USER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `USER` (
  `UNAME` varchar(127) NOT NULL,
  `UEMAIL` varchar(127) NOT NULL,
  PRIMARY KEY (`UEMAIL`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `USER`
--

LOCK TABLES `USER` WRITE;
/*!40000 ALTER TABLE `USER` DISABLE KEYS */;
INSERT INTO `USER` VALUES ('Alice 黃','123456@gmail.com'),('李承恩','Andy@gmail.com'),('承恩李','Andy2@gmail.com');
/*!40000 ALTER TABLE `USER` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-06-07 14:54:29
